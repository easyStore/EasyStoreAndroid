package hu.kleatech.easystore.camera;

import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.Gravity;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;

import hu.kleatech.easystore.common.ActivityWithMessageSupport;
import hu.kleatech.easystore.R;
import hu.kleatech.easystore.common.State;
import hu.kleatech.easystore.common.StateGlobal;

import static hu.kleatech.easystore.utils.Utils.async;
import static hu.kleatech.easystore.utils.Utils.ignoreEx;

public class Camera extends AppCompatActivity implements ActivityWithMessageSupport {

    public static final class StateLocal extends State {
        public static volatile Boolean editing = false;
        public static volatile Collection<String> imagesBase64 = new ArrayList<>(3);
        static volatile Bitmap snapshot;
        static volatile LinkedHashMap<View, Bitmap> gallery = new LinkedHashMap<>();
        public static void clearState() {
            snapshot = null;
            gallery.clear();
            editing = false;
            imagesBase64.clear();
        }
    }

    private SurfaceView cameraView;
    private TextView textView;
    private Button saveSerialButton;
    private Button takePhotoButton;
    private LinearLayout galleryLayout;

    private QRCamera qrCamera;
    private Bitmap snapshot;

    private void wireView() {
        cameraView = findViewById(R.id.cameraView);
        textView = findViewById(R.id.textView);
        saveSerialButton = findViewById(R.id.save_serial_button);
        takePhotoButton = findViewById(R.id.take_photo_button);
        galleryLayout = findViewById(R.id.gallery);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_camera);
        wireView();
        if (StateLocal.editing) saveSerialButton.setEnabled(false);
        qrCamera = new QRCamera(cameraView.getHolder(), this);
        saveSerialButton.setOnClickListener((btn) -> {
            if (qrCamera.isFound()) {
                qrCamera.stopCapture();
                StateGlobal.QRRawSerial = textView.getText().toString();
            }
        });
        takePhotoButton.setOnClickListener((btn) -> async(() -> ignoreEx(() -> {
            StateLocal.snapshot = null;
           snapshot = qrCamera.takeSnapshot();
           View imageView = generateImageView(snapshot);
           StateLocal.gallery.put(imageView, snapshot);
           imageView.setOnClickListener((img) -> {
               StateLocal.gallery.remove(img);
               ((ViewManager)img.getParent()).removeView(img);
           });
           galleryLayout.post(() -> galleryLayout.addView(imageView));
        })));
        if (StateLocal.editing) {
            for (String img : StateLocal.imagesBase64) {
                base64ToBitmap(img);
                View imageView = generateImageView(StateLocal.snapshot);
                StateLocal.gallery.put(imageView, StateLocal.snapshot);
                imageView.setOnClickListener((i) -> {
                    StateLocal.gallery.remove(i);
                    ((ViewManager)i.getParent()).removeView(i);
                });
            }
            StateLocal.imagesBase64.clear();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        qrCamera.stopPreview();
        StateGlobal.galery.clear();
        StateGlobal.galery.addAll(StateLocal.gallery.values());
        for (View view : StateLocal.gallery.keySet()) {
            ((ViewManager) view.getParent()).removeView(view);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        for (View view : StateLocal.gallery.keySet()) galleryLayout.addView(view);
        async(() -> {
            qrCamera.startPreview();
            if (!StateLocal.editing) async(() -> qrCamera.startCapture(500));
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public TextView getMessageView() {
        return textView;
    }

    private View generateImageView(Bitmap bitmap) {
        LinearLayout layout = new LinearLayout(getApplicationContext());
        layout.setLayoutParams(new LinearLayout.LayoutParams(250, 210));
        layout.setGravity(Gravity.CENTER);

        ImageView imageView = new ImageView(getApplicationContext());
        imageView.setLayoutParams(new LinearLayout.LayoutParams(240, 180));
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setImageBitmap(bitmap);

        layout.addView(imageView);
        return layout;
    }

    private void base64ToBitmap(String base64image) {
        byte[] decodedString = Base64.decode(base64image, Base64.DEFAULT);
        Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        StateLocal.snapshot = bitmap;
    }
}
