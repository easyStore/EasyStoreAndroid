package hu.kleatech.easystore.camera;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.support.v8.renderscript.RenderScript;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.widget.TextView;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.util.concurrent.atomic.AtomicBoolean;

import hu.kleatech.easystore.common.ActivityWithMessageSupport;
import hu.kleatech.easystore.R;
import io.github.silvaren.easyrs.tools.Nv21Image;
import hu.kleatech.easystore.camera.Camera.StateLocal;

import static hu.kleatech.easystore.utils.Utils.*;

public class QRCamera {

    private final SurfaceHolder surfaceHolder;
    protected Camera camera;
    private BarcodeDetector detector;
    private TextView messageView;
    private AtomicBoolean capture;
    private Barcode foundOne = null;

    protected RenderScript rs;

    private Runnable onFoundOneListener = () -> {};

    protected QRCamera(){surfaceHolder=null;}
    public QRCamera(SurfaceHolder surfaceHolder, ActivityWithMessageSupport caller) {
        rs = RenderScript.create(caller.getApplicationContext());
        capture = new AtomicBoolean(false);
        this.surfaceHolder = surfaceHolder;
        this.messageView = caller.getMessageView();
        detector = new BarcodeDetector.Builder(caller.getApplicationContext())
                .setBarcodeFormats(Barcode.DATA_MATRIX | Barcode.QR_CODE)
                .build();
        if (!detector.isOperational()) {
            caller.getMessageView().setText(R.string.detector_not_operational);
        }
    }

    public void setOnFoundOneListener(Runnable runnable) {
        onFoundOneListener = runnable;
    }

    public void startPreview() {
        ignoreEx(() -> camera = Camera.open());
        Camera.Parameters params = camera.getParameters();
        //for (Camera.Size size : params.getSupportedPreviewSizes()) Log.e("Supported size", size.width + "x" + size.height);
        params.setPreviewSize(640, 480);
        camera.setParameters(params);
        camera.setDisplayOrientation(90);
        ignoreEx(() -> camera.setPreviewDisplay(surfaceHolder));
        camera.startPreview();
    }

    public void stopPreview() {
        stopCapture();
        nullsafe(() -> camera.release());
        camera = null;
    }

    public void startCapture(int interval) {
        class capturer {
            Bitmap bitmap;
            Frame frame;
            SparseArray<Barcode> barcodes;
            void run() throws Exception {
                while (capture.get()) {
                    camera.setOneShotPreviewCallback((bytes, camera) -> { try {
                        int width = camera.getParameters().getPreviewSize().width;
                        int height = camera.getParameters().getPreviewSize().height;
                        bitmap = Nv21Image.nv21ToBitmap(rs, bytes, width, height);
                        frame = new Frame.Builder().setBitmap(bitmap).build();
                        barcodes = detector.detect(frame);
                        try {
                            foundOne = barcodes.valueAt(0);
                            Log.i("Result", foundOne.rawValue);
                            messageView.setText(foundOne.rawValue);
                            onFoundOneListener.run();
                        }
                        catch (ArrayIndexOutOfBoundsException e) {
                            if (!isFound()) {
                                Log.i("Result", "No QR code found.");
                                messageView.setText("No QR code found.");
                            }
                        }
                    } catch (Exception e) {} });
                    ignoreEx(() -> Thread.sleep(interval));
                }
            }
        }
        capture.set(true);
        async(() -> ignoreEx(() -> new capturer().run()));
    }

    public Bitmap takeSnapshot() throws RuntimeException {
        camera.setOneShotPreviewCallback(((bytes, camera1) -> {
            int width = camera.getParameters().getPreviewSize().width;
            int height = camera.getParameters().getPreviewSize().height;
            StateLocal.snapshot = Nv21Image.nv21ToBitmap(rs, bytes, width, height);
            Log.i("Snapshot taken", StateLocal.snapshot==null?"null":"OK");
        }));
        Bitmap ret = poll(() -> {return StateLocal.snapshot;}, null, 50, 1000);
        StateLocal.snapshot = null;
        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        ret = Bitmap.createBitmap(ret, 140, 0, 360, 480, matrix, true);
        return ret;
    }

    public void stopCapture() {
        capture.set(false);
    }

    public boolean isFound() {
        return foundOne!=null;
    }

    public String getRaw() {
        return foundOne.rawValue;
    }

}
