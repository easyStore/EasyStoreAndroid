package hu.kleatech.easystore;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;

import hu.kleatech.easystore.compartment.Compartments;
import hu.kleatech.easystore.sender.Sender;
import static hu.kleatech.easystore.utils.Utils.*;

public class LaunchActivity extends AppCompatActivity {

    private EditText serverAddress;
    private TextView serverStatus;
    private Button saveButton;
    private Button compartmentsButton;

    private void wireView() {
        serverAddress = findViewById(R.id.server_address);
        serverStatus = findViewById(R.id.server_status);
        saveButton = findViewById(R.id.save_button);
        compartmentsButton = findViewById(R.id.compartments_button);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        wireView();
        SharedProperties.setContext(this);
        serverAddress.setText(SharedProperties.getProperty("server-address"));
        serverStatus.setText("Checking server status...");
        async(() -> testConnection());
        saveButton.setEnabled(false);
        saveButton.setOnClickListener((btn) -> {
            SharedProperties.setProperty("server-address", serverAddress.getText().toString());
            serverStatus.setText("Checking server status...");
            async(() -> testConnection());
            saveButton.setEnabled(false);
        });
        compartmentsButton.setOnClickListener((btn) -> {
            Intent compartmentsActivity = new Intent(this, Compartments.class);
            startActivity(compartmentsActivity);
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    private void testConnection() {
        try {
            String response = new Sender(serverAddress.getText().toString() + "/test").sendPostAsText("Testing");
            if (nullsafe(() -> response.contains("Success"), false)) {
                serverStatus.post(() -> serverStatus.setText("Server status: Up and running."));
            }
            else throw new IOException(nullsafe(() -> response, ""));
        }
        catch (IOException e) {
            serverStatus.post(() -> serverStatus.setText("No running server has found on this address. " + e.getMessage()));
        };
        saveButton.post(() -> saveButton.setEnabled(true));
    }
}
