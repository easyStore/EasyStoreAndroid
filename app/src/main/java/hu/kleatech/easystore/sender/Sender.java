package hu.kleatech.easystore.sender;

import android.net.Uri;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import com.cedarsoftware.util.io.JsonWriter;

import hu.kleatech.easystore.common.Model;

import static hu.kleatech.easystore.utils.Utils.*;

public class Sender {
    private String address;

    public Sender(String address) {
        this.address = address;
    }

    public String send(Model.CompartmentTransfer compartment) throws IOException {
        Map<String, Object> optArgs = new HashMap<>(1);
        optArgs.put(JsonWriter.TYPE, false);
        return sendPostAsJson(JsonWriter.objectToJson(compartment, optArgs), address);
    }

    public String send(Model.ElemTransfer elem) throws IOException {
        Map<String, Object> optArgs = new HashMap<>(1);
        optArgs.put(JsonWriter.TYPE, false);
        return sendPostAsJson(JsonWriter.objectToJson(elem, optArgs), address);
    }

    public String send(String params) throws IOException {
        return sendGet(address, params);
    }

    protected String sendPostAsText(String data, String address)  throws IOException {
        if(!address.startsWith("http")) address = "http://" + address;
        URL url = new URL(address);
        URLConnection con = url.openConnection();
        HttpURLConnection http = (HttpURLConnection)con;
        http.setConnectTimeout(1000);
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        byte[] out = data.getBytes("UTF-8");
        http.setFixedLengthStreamingMode(out.length);
        http.setRequestProperty("Content-Type", "application/form-data; charset=UTF-8");
        http.connect();
        OutputStream os = http.getOutputStream();
        os.write(out);
        os.close();
        String ret = convertStreamToString(http.getInputStream());
        Log.w("Response", ret);
        http.getInputStream().close();
        http.disconnect();
        return ret;
    }

    protected String sendPostAsJson(String data, String address)  throws IOException {
        print(data.substring(0, Math.min(50, data.length())));
        if(!address.startsWith("http")) address = "http://" + address;
        URL url = new URL(address);
        URLConnection con = url.openConnection();
        HttpURLConnection http = (HttpURLConnection)con;
        http.setConnectTimeout(1000);
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        byte[] out = data.getBytes("UTF-8");
        http.setFixedLengthStreamingMode(out.length);
        http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        http.connect();
        OutputStream os = http.getOutputStream();
        os.write(out);
        os.close();
        String ret = either(
                () -> convertStreamToString(http.getInputStream()),
                () -> convertStreamToString(http.getErrorStream()));
        Log.i("Response", ret);
        either(
                () -> http.getInputStream().close(),
                () -> http.getErrorStream().close()
        );
        http.disconnect();
        return ret;
    }

    public String sendPostAsText(String text) throws IOException {
        return sendPostAsText(text, address);
    }

    public String sendPostAsJson(String json) throws IOException {
        return sendPostAsJson(json, address);
    }

    public String sendGet(String address, String params) throws IOException {
        String[] keyValuePairs = params.split("&");
        if(!address.startsWith("http")) address = "http://" + address;
        Uri.Builder uri = Uri.parse(address).buildUpon();
        for (String str : keyValuePairs) {
            String[] tmp = str.split("=");
            uri.appendQueryParameter(tmp[0], tmp[1]);
        }
        //URL url = new URL(address + '?' + params);
        URL url = new URL(uri.build().toString());
        Log.e("URL", url.toString());
        HttpURLConnection http = (HttpURLConnection) url.openConnection();
        http.setConnectTimeout(1000);
        http.setRequestMethod("GET");
        http.setRequestProperty("User-Agent", "Android");
        String ret = convertStreamToString(http.getInputStream());
        Log.w("Response", ret);
        http.getInputStream().close();
        http.disconnect();
        return ret;
    }
}
