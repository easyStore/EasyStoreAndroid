package hu.kleatech.easystore.common;

import java.util.List;
import java.util.Properties;
import java.util.Set;

public class Model {
    public static class CompartmentTransfer implements Dto{
        private String serial;
        private String name;
        private String room;
        private String shelf;
        private List<String> imagesBase64;
        private Properties infos;
        private Set<String> tags;
        public CompartmentTransfer(){}
        public void setSerial(String serial) { this.serial = serial; }
        public void setName(String name) { this.name = name; }
        public void setRoom(String room) { this.room = room; }
        public void setShelf(String shelf) { this.shelf = shelf; }
        public void setInfos(Properties infos) { this.infos = infos; }
        public void setImagesBase64(List<String> imagesBase64) { this.imagesBase64 = imagesBase64; }
        public void setTags(Set<String> tags) { this.tags = tags; }
        @Override
        public String getSerial() { return serial; }
        public String getName() { return name; }
        public String getRoom() { return room; }
        public String getShelf() { return shelf; }
        public Properties getInfos() { return infos; }
        @Override
        public List<String> getImagesBase64() { return imagesBase64; }
        public Set<String> getTags() { return tags; }
        @Override
        public String toString() {
            return "CompartmentTransfer{" + "serial=" + serial + ", name=" + name + ", room=" + room + ", shelf=" + shelf + ", infos=" + infos + ", noOfPics=" + imagesBase64.size() + ", tags=" + tags + '}';
        }
    }
    public static class ElemTransfer implements Dto {
        private String name;
        private Double value;
        private String desc;
        private String status;
        private String serial;
        private String compartmentSerial;
        private Properties infos;
        private List<String> imagesBase64;
        private Set<String> tags;
        public ElemTransfer(){}
        public void setName(String name) { this.name = name; }
        public void setValue(Double value) { this.value = value; }
        public void setDesc(String desc) { this.desc = desc; }
        public void setStatus(String status) { this.status = status; }
        public void setSerial(String serial) { this.serial = serial; }
        public void setCompartmentSerial(String compartmentSerial) { this.compartmentSerial = compartmentSerial; }
        public void setInfos(Properties infos) { this.infos = infos; }
        public void setImagesBase64(List<String> imagesBase64) { this.imagesBase64 = imagesBase64; }
        public void setTags(Set<String> tags) { this.tags = tags; }
        public String getName() { return name; }
        public Double getValue() { return value; }
        public String getDesc() { return desc; }
        public String getStatus() { return status; }
        @Override
        public String getSerial() { return serial; }
        public String getCompartmentSerial() { return compartmentSerial; }
        public Properties getInfos() { return infos; }
        @Override
        public List<String> getImagesBase64() { return imagesBase64; }
        public Set<String> getTags() { return tags; }
        @Override
        public String toString() {
            return "ElemTransfer{" + "name=" + name + ", value=" + value + ", desc=" + desc + ", status=" + status + ",  serial=" + serial + ", compartmentSerial=" + compartmentSerial + ", infos=" + infos + ", noOfPics=" + imagesBase64.size() + ", tags=" + tags + '}';
        }
    }
    public interface Dto {
        String getSerial();
        List<String> getImagesBase64();
    }
}
