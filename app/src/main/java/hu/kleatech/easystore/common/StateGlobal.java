package hu.kleatech.easystore.common;

import android.graphics.Bitmap;

import java.util.LinkedHashSet;

public final class StateGlobal extends State {
    private StateGlobal(){}

    public static volatile String QRRawSerial = null;
    public static volatile LinkedHashSet<Bitmap> galery = new LinkedHashSet<>();

    @OverrideStatic
    public static void clearState() {
        QRRawSerial = null;
        galery.clear();
    }
}
