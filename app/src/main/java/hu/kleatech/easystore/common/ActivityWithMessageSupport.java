package hu.kleatech.easystore.common;

import android.content.Context;
import android.widget.TextView;

public interface ActivityWithMessageSupport {

    Context getApplicationContext();
    TextView getMessageView();
}
