package hu.kleatech.easystore.common;

import java.lang.reflect.Field;
import static hu.kleatech.easystore.utils.Utils.*;

public abstract class State {
    public static void clearState(Class clazz) {
        for (Field field : clazz.getFields()) ignoreEx(() -> field.set(null, null));
    };
}
