package hu.kleatech.easystore.search;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import static hu.kleatech.easystore.utils.Utils.*;

import hu.kleatech.easystore.R;
import hu.kleatech.easystore.sender.Sender;

public class ElemSearch extends AppCompatActivity {

    private TableLayout table;
    private Spinner dropdown;
    private EditText criteria;
    private Button searchButton;

    private TableBuilderElem tableBuilder;
    private String json;

    private void wireView() {
        this.table = findViewById(R.id.elem_search_table);
        this.dropdown = findViewById(R.id.elem_search_dropdown);
        this.criteria = findViewById(R.id.elem_search_text_field);
        this.searchButton = findViewById(R.id.elem_search_btn);
    }

    private void initDropdown() {
        ArrayAdapter temp = ArrayAdapter.createFromResource(this,
                R.array.elem_search_criterias,
                android.R.layout.simple_spinner_item);
        temp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dropdown.setAdapter(temp);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_elem_search);
        wireView();
        initDropdown();
        tableBuilder = new TableBuilderElem(table);
        searchButton.setOnClickListener(btn -> {
            Sender sender = new Sender(SharedProperties.getProperty("server-address") + "/elem/query");
            btn.setEnabled(false);
            tryThis(() -> {
                async(() -> ignoreEx(() -> json = sender.send(dropdown.getSelectedItem().toString().toLowerCase() + "=" + criteria.getText().toString())));
                poll(() -> {return json;}, null, 50);
                tableBuilder.build(json);
                json = null;
                btn.post(() -> btn.setEnabled(true));
            }).orElse(ex -> btn.setEnabled(true));
        });
    }
}
