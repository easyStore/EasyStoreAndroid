package hu.kleatech.easystore.utils;

import android.content.Context;
import android.util.Log;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public final class Utils {

    public static <T> Else<T> tryThis(ProblematicSupplier<T> method) {
        try {
            T temp = method.call();
            return (t) -> temp;
        } catch (Exception e) {
            Log.w("tryThis catched", e);
            return (t) -> t;
        }
    }

    public static <S, A> Else<S> tryThis(A obj, ProblematicSupplierWithArgument<S, A> method) {
        try {
            S temp = method.call(obj);
            return t -> temp;
        } catch (Exception e) {
            Log.w("tryThis catched", e);
            return t -> t;
        }
    }

    public static Catch<Exception> tryThis(Command command) {
        try {
            command.execute();
            return co -> {
            };
        } catch (Exception ex) {
            return co -> co.execute(ex);
        }
    }

    public static String convertStreamToString(InputStream is) throws IOException {
        return CharStreams.toString(new InputStreamReader(is, Charsets.UTF_8));
    }

    public static void nullsafe(Command command) {
        try {
            command.execute();
        } catch (NullPointerException e) {
            Log.d("Ignored NullpointerEx", command.getClass().getName() + " was null.");
        } catch (Exception e) {
            Log.w("Ignored Ex", "Nullsafe method catched an unexpected exception.", e);
        }
    }

    public static <T> T nullsafe(Supplier<T> method, T insteadNull) {
        T val = method.call();
        if (val == null) return insteadNull;
        return val;
    }

    public static void ignoreEx(Command command) {
        try {
            command.execute();
        } catch (Exception e) {
            Log.w("Ignored Ex", "IgnoreEx method cathced an exception: " + e.getMessage(), e);
        }
    }

    public static <T> void ignoreEx(T obj, CommandWithArgument<T> command) {
        try {
            command.execute(obj);
        } catch (Exception e) {
            Log.w("Ignored Ex", "IgnoreEx method cathced an exception: " + e.getMessage(), e);
        }
    }

    public static void delay(int ms) {
        ignoreEx(() -> Thread.sleep(ms));
    }

    public static Thread async(Runnable runnable) {
        Thread thread = new Thread(runnable);
        thread.start();
        return thread;
    }

    public static <T> T poll(Supplier<T> method, T notReadyMarker, int ms, int... limitDefault5sec) {
        int limit = limitDefault5sec.length == 0 ? 5000 : limitDefault5sec[0];
        while (limit > 0) {
            limit -= ms;
            Log.d("Utils", "Polling...");
            T actual = method.call();
            if (actual == null) {
                if (notReadyMarker == null) delay(ms);
                else return null;
            } else if (actual.equals(notReadyMarker)) {
                delay(ms);
            } else return actual;
        }
        throw new RuntimeException("Polling limit exceeded");
    }

    @SafeVarargs
    public static <T> T print(T... obj) {
        StringBuilder builder = new StringBuilder();
        for (Object o : obj) {
            builder.append(String.valueOf(o));
            builder.append(' ');
        }
        Log.i("INFO", builder.toString());
        return obj[0];
    }

    public static <T> T either(ProblematicSupplier<T> first, ProblematicSupplier<T> second) {
        try {
            try {
                return first.call();
            }
            catch (Exception e) {
                return second.call();
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void either(Command first, Command second) {
        try {
            first.execute();
        }
        catch (Exception e) {
            try {
                second.execute();
            }
            catch (Exception ex) {
                Log.w("Either catched", ex);
            }
        }
    }

    @FunctionalInterface
    public interface SimpleCommandWithArgument<T> {
        void execute(T obj);
    }

    @FunctionalInterface
    public interface Command {
        void execute() throws Exception;
    }

    @FunctionalInterface
    public interface CommandWithArgument<T> {
        void execute(T obj) throws Exception;
    }

    @FunctionalInterface
    public interface Supplier<T> {
        T call();
    }

    @FunctionalInterface
    public interface ProblematicSupplier<T> {
        T call() throws Exception;
    }

    @FunctionalInterface
    public interface ProblematicSupplierWithArgument<S, A> {
        S call(A obj) throws Exception;
    }

    @FunctionalInterface
    public interface Else<T> {
        T orElse(T t);
    }

    @FunctionalInterface
    public interface Catch<E extends Exception> {
        void orElse(SimpleCommandWithArgument<Exception> command);
    }

    public static class PlaceholderException extends Exception {
    }

    public static class SharedProperties {

        private static Context context = null;

        private SharedProperties() {
        }

        public static void setContext(Context context) {
            SharedProperties.context = context.getApplicationContext();
        }

        public static String getProperty(String key) {
            if (context == null) throw new IllegalStateException("Context is not initialized.");
            return context.getSharedPreferences("hu.kleatech.easystore", Context.MODE_PRIVATE).getString(key, "");
        }

        public static void setProperty(String key, String value) {
            if (context == null) throw new IllegalStateException("Context is not initialized.");
            context.getSharedPreferences("hu.kleatech.easystore", Context.MODE_PRIVATE).edit().putString(key, value).apply();
        }
    }
}
