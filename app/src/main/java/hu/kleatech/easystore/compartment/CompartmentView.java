package hu.kleatech.easystore.compartment;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import hu.kleatech.easystore.R;
import hu.kleatech.easystore.camera.Camera;
import hu.kleatech.easystore.common.ActivityWithMessageSupport;
import hu.kleatech.easystore.common.Model;
import hu.kleatech.easystore.common.OverrideStatic;
import hu.kleatech.easystore.common.State;
import hu.kleatech.easystore.common.StateGlobal;
import hu.kleatech.easystore.common.ValidationError;
import hu.kleatech.easystore.elem.Elems;
import hu.kleatech.easystore.sender.Sender;
import hu.kleatech.easystore.utils.Utils;

import static hu.kleatech.easystore.utils.Utils.*;

public class CompartmentView extends AppCompatActivity implements ActivityWithMessageSupport {

    public static final class StateLocal extends State {
        private StateLocal(){}
        public static volatile Boolean editing = false;
        public static volatile Model.CompartmentTransfer existingCompartment_;
        public static volatile Runnable atNextResume = () -> {};
        public static void setExistingCompartment(Model.CompartmentTransfer existingCompartment) {
            existingCompartment_=existingCompartment;
        }
        public static Model.CompartmentTransfer getExistingCompartment() {
            return existingCompartment_;
        }
        @OverrideStatic
        public static void clearState() {
            editing = false;
            setExistingCompartment(null);
            atNextResume = () -> {};
        }
    }

    private EditText serial;
    private Button openCameraButton;
    private TextView numberOfPictures;
    private EditText name;
    private Button sendButton;
    private EditText room;
    private EditText shelf;
    private Intent QRCameraActivity;
    private Button elemsButton;
    private TextView message;
    private Button deleteButton;

    private void wireView() {
        serial = findViewById(R.id.serial);
        openCameraButton = findViewById(R.id.open_camera_button);
        numberOfPictures = findViewById(R.id.number_of_pictures);
        name = findViewById(R.id.name);
        sendButton = findViewById(R.id.send_button);
        room = findViewById(R.id.room);
        shelf = findViewById(R.id.shelf);
        elemsButton = findViewById(R.id.elems_button);
        message = findViewById(R.id.comp_view_message);
        deleteButton = findViewById(R.id.comp_del_btn);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_compartment_view);
        wireView();
        elemsButton.setEnabled(false);
        if (StateLocal.editing) load();
        openCameraButton.setOnClickListener((btn) -> {
            QRCameraActivity = new Intent(this, hu.kleatech.easystore.camera.Camera.class);
            startActivity(QRCameraActivity);
        });
        sendButton.setOnClickListener((btn) -> {
            async(() -> ignoreEx(() -> {
                Sender sender = new Sender(SharedProperties.getProperty("server-address") +
                        (StateLocal.editing?"/compartment/save":"/compartment/add"));
                Model.CompartmentTransfer compartment = new Model.CompartmentTransfer();
                compartment.setSerial(serial.getText().toString());
                compartment.setName(name.getText().toString());
                compartment.setImagesBase64(imagesBase64(StateGlobal.galery));
                compartment.setRoom(room.getText().toString());
                compartment.setShelf(shelf.getText().toString());
                String response = sender.send(compartment);
                if (response.contains("Success")) {
                    StateLocal.clearState();
                    Camera.StateLocal.clearState();
                    ignoreEx(() -> stopService(QRCameraActivity));
                    StateGlobal.galery.clear();
                    finish();
                }
                else {
                    message.post(() -> message.setText(""));
                    serial.post(() -> serial.setBackgroundColor(Color.WHITE));
                    name.post(() -> name.setBackgroundColor(Color.WHITE));
                    print("at else", response);
                    ObjectMapper mapper = new ObjectMapper();
                    List<ValidationError> errors = Utils.<List<ValidationError>>tryThis(() -> mapper.readValue(response, new TypeReference<List<ValidationError>>(){}))
                            .orElse(null);
                    StringBuilder builder = new StringBuilder();
                    for (ValidationError error : errors) {
                        builder.append(error.getField());
                        builder.append(' ');
                        builder.append(error.getMessage());
                        builder.append('\n');
                        if (error.getField().equals("serial")) {
                            serial.post(() -> serial.setBackgroundColor(Color.RED));
                        }
                        else if (error.getField().equals("name")) {
                            name.post(() -> name.setBackgroundColor(Color.RED));
                        }
                        message.post(() -> message.setText(builder.toString()));
                    }
                }
            }));
        });
        deleteButton.setOnClickListener(btn -> async(() -> ignoreEx(() -> {
            Sender sender = new Sender(SharedProperties.getProperty("server-address") + "/compartment/delete");
            String response = sender.send("serial=" + serial.getText().toString());
            if (response.contains("Success")) {
                StateLocal.clearState();
                Camera.StateLocal.clearState();
                ignoreEx(() -> stopService(QRCameraActivity));
                StateGlobal.galery.clear();
                finish();
            }}
        )));
        elemsButton.setOnClickListener((btn) -> {
            async(() -> ignoreEx(() -> {
                Sender sender = new Sender(SharedProperties.getProperty("server-address") +
                        (StateLocal.editing?"/compartment/save":"/compartment/add"));
                Model.CompartmentTransfer compartment = new Model.CompartmentTransfer();
                compartment.setSerial(serial.getText().toString());
                compartment.setName(name.getText().toString());
                compartment.setImagesBase64(imagesBase64(StateGlobal.galery));
                compartment.setRoom(room.getText().toString());
                compartment.setShelf(shelf.getText().toString());
                String response = sender.send(compartment);
                if (response.contains("Success")) {
                    Camera.StateLocal.clearState();
                    Intent elemsActivity = new Intent(this, Elems.class);
                    Elems.StateLocal.compSerial = serial.getText().toString();
                    Camera.StateLocal.clearState();
                    ignoreEx(() -> stopService(QRCameraActivity));
                    StateGlobal.galery.clear();
                    startActivity(elemsActivity);
                    StateLocal.atNextResume = () -> load();
                };
            }));
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        ignoreEx(() -> StateLocal.atNextResume.run());
        String QRRawSerial = StateGlobal.QRRawSerial;
        if (QRRawSerial!=null) serial.setText(QRRawSerial);
        numberOfPictures.setText(Math.max(Camera.StateLocal.imagesBase64.size(), StateGlobal.galery.size()) + " pictures stored.");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        StateLocal.clearState();
        Camera.StateLocal.clearState();
        ignoreEx(() -> stopService(QRCameraActivity));
        StateGlobal.galery.clear();
        finish();
    }

    @Override
    public TextView getMessageView() {
        return null;
    }

    private List<String> imagesBase64(Collection<Bitmap> images) {
        List<String> base64Images = new ArrayList<>(images.size());
        for (Bitmap bitmap : images) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 20, stream);
            base64Images.add(Base64.encodeToString(stream.toByteArray(), 0));
        }
        return base64Images;
    }

    private void load() {
        serial.setText(StateLocal.getExistingCompartment().getSerial());
        serial.setInputType(InputType.TYPE_NULL);
        serial.setFocusable(false);
        name.setText(StateLocal.getExistingCompartment().getName());
        room.setText(StateLocal.getExistingCompartment().getRoom());
        shelf.setText(StateLocal.getExistingCompartment().getShelf());
        Camera.StateLocal.editing = true;
        Camera.StateLocal.imagesBase64.addAll(StateLocal.getExistingCompartment().getImagesBase64());
        Log.e("bug", StateLocal.existingCompartment_.getImagesBase64().size()==0?"Possible bug at CompartmentView::load":"");
        elemsButton.setEnabled(true);
    }
}
