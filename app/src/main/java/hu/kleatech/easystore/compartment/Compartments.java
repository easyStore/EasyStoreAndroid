package hu.kleatech.easystore.compartment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TableLayout;
import static hu.kleatech.easystore.utils.Utils.*;

import hu.kleatech.easystore.R;
import hu.kleatech.easystore.sender.Sender;

public class Compartments extends AppCompatActivity {

    private Button upButton;
    private Button downButton;
    private TableLayout table;
    private TableBuilder tableBuilder;
    private Button addNewButton;
    private Button compSearchButton;
    private Button elemSearchButton;

    private String json;
    private int page = 0;
    private final int pageSize = 10;

    private void wireView() {
        upButton = findViewById(R.id.up_button);
        downButton = findViewById(R.id.down_button);
        table = findViewById(R.id.table);
        addNewButton = findViewById(R.id.add_new_button);
        compSearchButton = findViewById(R.id.comps_search_comp_btn);
        elemSearchButton = findViewById(R.id.comps_search_elem_btn);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compartments);
        wireView();
        tableBuilder = new TableBuilder(table);
        refreshTable();
        upButton.setOnClickListener((btn) -> {
            if (page!=0) {
                page--;
                refreshTable();
            }
        });
        downButton.setOnClickListener((btn) -> {
            page++;
            refreshTable();
        });
        addNewButton.setOnClickListener((btn) -> {
            Intent compartmentView = new Intent(this, hu.kleatech.easystore.compartment.CompartmentView.class);
            startActivity(compartmentView);
        });
        compSearchButton.setOnClickListener(btn -> {
            Intent compartmentSearchActivity = new Intent(this, hu.kleatech.easystore.search.CompartmentSearch.class);
            startActivity(compartmentSearchActivity);
        });
        elemSearchButton.setOnClickListener(btn -> {
            Intent elemSearchActivity = new Intent(this, hu.kleatech.easystore.search.ElemSearch.class);
            startActivity(elemSearchActivity);
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshTable();
    }

    private void refreshTable() {
        Sender sender = new Sender(SharedProperties.getProperty("server-address") + "/compartment/all");
        ignoreEx(() -> {
            async(() -> ignoreEx(() -> json = sender.send("page=" + page + "&pageSize=" + pageSize)));
            poll(() -> {return json;}, null, 50);
            tableBuilder.build(json);
        });
        json = null;
    }
}
