package hu.kleatech.easystore.elem;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TableLayout;

import hu.kleatech.easystore.R;
import hu.kleatech.easystore.sender.Sender;
import hu.kleatech.easystore.utils.Utils;

import static hu.kleatech.easystore.utils.Utils.*;

public class Elems extends AppCompatActivity {

    public static final class StateLocal{
        public static String compSerial;
        public static void clearState() {
            compSerial = null;
        }
    }

    private Button upButton;
    private Button downButton;
    private TableLayout table;
    private TableBuilder tableBuilder;
    private Button addNewButton;

    private String json;
    private int page = 0;
    private final int pageSize = 5;

    private void wireView() {
        upButton = findViewById(R.id.elems_up_button);
        downButton = findViewById(R.id.elems_down_button);
        table = findViewById(R.id.elems_table);
        addNewButton = findViewById(R.id.elems_add_new_button);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_elems);
        wireView();
        tableBuilder = new TableBuilder(table);
        refreshTable();
        upButton.setOnClickListener((btn) -> {
            if (page!=0) {
                page--;
                refreshTable();
            }
        });
        downButton.setOnClickListener((btn) -> {
            page++;
            refreshTable();
        });
        addNewButton.setOnClickListener((btn) -> {
            Intent elemView = new Intent(this, hu.kleatech.easystore.elem.ElemView.class);
            startActivity(elemView);
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshTable();
    }

    private void refreshTable() {
        Sender sender = new Sender(Utils.SharedProperties.getProperty("server-address") + "/elem/all");
        ignoreEx(() -> {
            async(() -> ignoreEx(() -> json = sender.send("compSerial="+ StateLocal.compSerial + "&page=" + page + "&pageSize=" + pageSize)));
            poll(() -> {return json;}, null, 50);
            tableBuilder.build(json);
        });
        json = null;
    }
}
