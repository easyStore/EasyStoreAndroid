package hu.kleatech.easystore.elem;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Base64;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import hu.kleatech.easystore.R;
import hu.kleatech.easystore.camera.Camera;
import hu.kleatech.easystore.common.ActivityWithMessageSupport;
import hu.kleatech.easystore.common.Model;
import hu.kleatech.easystore.common.OverrideStatic;
import hu.kleatech.easystore.common.State;
import hu.kleatech.easystore.common.StateGlobal;
import hu.kleatech.easystore.common.ValidationError;
import hu.kleatech.easystore.compartment.CompartmentView;
import hu.kleatech.easystore.sender.Sender;
import hu.kleatech.easystore.utils.Utils;

import static hu.kleatech.easystore.utils.Utils.*;

public class ElemView extends AppCompatActivity implements ActivityWithMessageSupport{

    public static final class StateLocal extends State {
        private StateLocal(){}
        public static volatile Boolean editing = false;
        public static volatile Model.ElemTransfer existingElem;
        @OverrideStatic
        public static void clearState() {
            editing = false;
            existingElem = null;
        }
    }

    private EditText desc;
    private Button generateButton;
    private EditText name;
    private TextView numberOfPics;
    private Button cameraButton;
    private EditText serial;
    private EditText status;
    private EditText value;
    private Button sendButton;
    private Intent QRCameraActivity;
    private TextView message;
    private Button delButton;

    private void wireView() {
        desc = findViewById(R.id.elem_desc);
        generateButton = findViewById(R.id.elem_generate_serial_button);
        name = findViewById(R.id.elem_name);
        numberOfPics = findViewById(R.id.elem_number_of_pictures);
        cameraButton = findViewById(R.id.elem_open_camera_button);
        serial = findViewById(R.id.elem_serial);
        status = findViewById(R.id.elem_status);
        value = findViewById(R.id.elem_value);
        sendButton = findViewById(R.id.elem_send_button);
        message = findViewById(R.id.elem_view_message);
        delButton = findViewById(R.id.elem_delete_btn);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_elem_view);
        wireView();
        if (StateLocal.editing) load();
        generateButton.setOnClickListener(btn -> {
            if (!serial.isFocusable()) return;
            Sender sender = new Sender(SharedProperties.getProperty("server-address") + "/elem/genSerial");
            async(() -> ignoreEx(() -> {
                String result = sender.send("compSerial=" + CompartmentView.StateLocal.getExistingCompartment().getSerial());
                serial.post(() -> serial.setText(result));
            }));
        });
        cameraButton.setOnClickListener(btn -> {
            QRCameraActivity = new Intent(this, hu.kleatech.easystore.camera.Camera.class);
            startActivity(QRCameraActivity);
        });
        sendButton.setOnClickListener(btn -> async(() -> ignoreEx(() -> {
            Sender sender = new Sender(SharedProperties.getProperty("server-address") +
                    (StateLocal.editing?"/elem/save":"/elem/add"));
            Model.ElemTransfer elem = new Model.ElemTransfer();
            elem.setSerial(serial.getText().toString());
            elem.setName(name.getText().toString());
            elem.setImagesBase64(imagesBase64(StateGlobal.galery));
            elem.setDesc(desc.getText().toString());
            elem.setStatus(status.getText().toString());
            elem.setValue(tryThis(() -> Double.parseDouble(value.getText().toString())).orElse(0d));
            String serial;
            if (Elems.StateLocal.compSerial!=null) serial=Elems.StateLocal.compSerial;
            else serial = StateLocal.existingElem.getCompartmentSerial();
            elem.setCompartmentSerial(serial);
            String response = sender.send(elem);
            if (response.contains("Success")) {
                StateLocal.clearState();
                Camera.StateLocal.clearState();
                ignoreEx(() -> stopService(QRCameraActivity));
                StateGlobal.galery.clear();
                finish();
            }
            else {
                message.post(() -> message.setText(""));
                this.serial.post(() -> this.serial.setBackgroundColor(Color.WHITE));
                name.post(() -> name.setBackgroundColor(Color.WHITE));
                print("at else", response);
                ObjectMapper mapper = new ObjectMapper();
                List<ValidationError> errors = Utils.<List<ValidationError>>tryThis(() -> mapper.readValue(response, new TypeReference<List<ValidationError>>(){}))
                        .orElse(null);
                StringBuilder builder = new StringBuilder();
                for (ValidationError error : errors) {
                    builder.append(error.getField());
                    builder.append(' ');
                    builder.append(error.getMessage());
                    builder.append('\n');
                    if (error.getField().equals("serial")) {
                        this.serial.post(() -> this.serial.setBackgroundColor(Color.RED));
                    }
                    else if (error.getField().equals("name")) {
                        name.post(() -> name.setBackgroundColor(Color.RED));
                    }
                    message.post(() -> message.setText(builder.toString()));
                }
            }
        })));
        delButton.setOnClickListener(btn -> {
            Sender sender = new Sender(SharedProperties.getProperty("server-address") + "/elem/delete");
            String compSerial;
            if (Elems.StateLocal.compSerial!=null) compSerial=Elems.StateLocal.compSerial;
            else compSerial = StateLocal.existingElem.getCompartmentSerial();
            async(() -> ignoreEx(() -> {
                String response = sender.send("compSerial=" + compSerial + "&serial=" + serial.getText().toString());
                if (response.contains("Success")) {
                    StateLocal.clearState();
                    Camera.StateLocal.clearState();
                    ignoreEx(() -> stopService(QRCameraActivity));
                    StateGlobal.galery.clear();
                    finish();
                }
                }));
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        String QRRawSerial = StateGlobal.QRRawSerial;
        if (QRRawSerial!=null) serial.setText(QRRawSerial);
        numberOfPics.setText(Math.max(StateGlobal.galery.size(), Camera.StateLocal.imagesBase64.size()) + " pictures stored.");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        StateLocal.clearState();
        Camera.StateLocal.clearState();
        ignoreEx(() -> stopService(QRCameraActivity));
        StateGlobal.galery.clear();
        finish();
    }

    @Override
    public TextView getMessageView() {
        return null;
    }

    private List<String> imagesBase64(Collection<Bitmap> images) {
        List<String> base64Images = new ArrayList<>(images.size());
        for (Bitmap bitmap : images) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 20, stream);
            base64Images.add(Base64.encodeToString(stream.toByteArray(), 0));
        }
        return base64Images;
    }

    private void load() {
        serial.setText(StateLocal.existingElem.getSerial());
        serial.setInputType(InputType.TYPE_NULL);
        serial.setFocusable(false);
        name.setText(StateLocal.existingElem.getName());
        desc.setText(StateLocal.existingElem.getDesc());
        status.setText(StateLocal.existingElem.getStatus());
        value.setText(StateLocal.existingElem.getValue().toString());
        Camera.StateLocal.editing = true;
        Camera.StateLocal.imagesBase64.addAll(StateLocal.existingElem.getImagesBase64());
    }
}
