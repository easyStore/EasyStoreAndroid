package hu.kleatech.easystore.elem;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import hu.kleatech.easystore.common.Model.ElemTransfer;
import hu.kleatech.easystore.sender.Sender;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

import static hu.kleatech.easystore.utils.Utils.*;

class TableBuilder {

    private TableLayout table;
    private Context context;

    TableBuilder(TableLayout table) {
        this.table = table;
        context = table.getContext();
    }

    void build(String json) throws IOException {
        table.removeAllViews();
        List<ElemTransfer> elems = new ObjectMapper().readValue(json, new TypeReference<List<ElemTransfer>>(){});
        Log.i("json", json.substring(0, Math.min(json.length(), 50)));
        table.addView(tableRow(textView("### "), textView("Name"), textView("Description  "), textView("Status    ")));
        table.addView(tableRow(textView(" "), textView(" "), textView(" "), textView(" ")));
        for (ElemTransfer elem : elems) {
            TableRow tableRow = tableRow(
                    textView(elem.getSerial()),
                    textView(elem.getName()),
                    textView(elem.getDesc()),
                    textView(elem.getStatus())
            );
            tableRow.setOnClickListener((row) -> async(() -> ignoreEx(() -> {
                ElemView.StateLocal.editing = true;
                Sender sender = new Sender(SharedProperties.getProperty("server-address") + "/elem");
                String result = sender.send("serial=" + serialFrom(row) + "&compSerial=" + elems.get(0).getCompartmentSerial());
                Log.i("json", result.substring(result.length()-50));
                ElemView.StateLocal.existingElem = new ObjectMapper().readValue(result, ElemTransfer.class);
                Intent elemViewActivity = new Intent(context, ElemView.class);
                context.startActivity(elemViewActivity);
            })));
            table.addView(tableRow);
        }
        table.setColumnStretchable(1, true);
    }

    private String serialFrom(View row) {
        return ((TextView) ((TableRow) row).getChildAt(0)).getText().toString();
    }

    private TextView textView(String text) {
        TextView textView = new TextView(context);
        textView.setText(text);
        textView.setTextSize(18);
        return textView;
    }

    private TableRow tableRow(View... views) {
        TableRow tableRow = new TableRow(context);
        for (View view : views) {
            tableRow.addView(view);
        }
        return tableRow;
    }
}
